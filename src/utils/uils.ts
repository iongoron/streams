export default class Utils {
    static arrayUnique(array:Array<any>) {
        var a = array.concat();
        for(var i=0; i<a.length; ++i) {
            for(var j=i+1; j<a.length; ++j) {
                if(a[i] === a[j])
                    a.splice(j--, 1);
            }
        }
    
        return a;
    }
    
    static arrayUniqueByParam(filterParam, array:Array<any>) {
        var a = array.concat();
        for(var i=0; i<a.length; ++i) {
            for(var j=i+1; j<a.length; ++j) {
                if(a[i][filterParam] === a[j][filterParam]){
                    a.splice(j--, 1);
                }
            }
        }
    
        return a;
    }
}