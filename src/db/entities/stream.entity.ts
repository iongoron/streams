import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity()
export class Stream {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column('bool', {
        default: true
    })
    _active: boolean;

    @Column('int')
    unit_weight!: number

    @Column('int')
    stream_product_id!: number

    @Column('varchar')
    type!: string

    @CreateDateColumn()
    _created: Date;

    @UpdateDateColumn()
    _modified: Date;

    @Column('varchar', { nullable: true })
    image!: string

    @Column('int')
    areacode: number

    @Column('varchar')
    postal_code_area: string

    @Column('json')
    availability: any;
    
    @Column('json')
    sizes: Object[];
    
    @Column('json')
    name: any;
    
    @Column('json')
    description: any;
}

