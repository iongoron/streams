import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn } from 'typeorm';

@Entity()
export class Container {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column('bool', {
        default: true
    })
    _active: boolean;

    @Column('int')
    size: number

    @Column('varchar')
    size_display: string

    @Column('int')
    container_product_id: number

    @Column('varchar')
    type: string

    @Column('varchar')
    image: string

    @Column('json')
    images!: string[]

    @Column('json')
    description!: any

    @Column('json')
    name!: any

    @CreateDateColumn()
    _modified: Date;

    @CreateDateColumn()
    _created: Date;

    @Column('float', { nullable: true })
    unit_price_purchase!: number

    @Column('float', { nullable: true })
    unit_price_rent!: number

    @Column('float', { nullable: true })
    unit_price_placement!: number

    @Column('float', { nullable: true })
    unit_price_pickup!: number

}

