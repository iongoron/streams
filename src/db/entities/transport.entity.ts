import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, BeforeInsert } from 'typeorm';

@Entity()
export class Transport {

    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column('varchar')
    provider: string

    @Column('json')
    supported_product_id: number[]

    @Column('json')
    supported_container_id: number[]

    @Column('int')
    postal_code_start: number
    
    @Column('int')
    postal_code_end: number

    @Column('varchar')
    postal_code_area: string

    @Column('json')
    availability: any

}

