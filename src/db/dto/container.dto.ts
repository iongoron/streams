export class ContainerInstance {
    _id: string;
    _active: boolean;
    size: number;
    size_display: string;
    container_product_id: number;
    type: string;
    image: string;
    images: string[];
    description: any;
    name: any;
    _created: Date;
    _modified: Date;
    unit_price_purchase:number;
    unit_price_rent:number;
    unit_price_placement:number;
}