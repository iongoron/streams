export class StreamInstance {
    _id: string;
    _active: boolean;
    unit_weight: number;
    stream_product_id: number;
    type: string;
    _created: Date;
    _modified: Date;
    image: string;
    areacode: number;
    postal_code_area: string;
    availability: any;
    sizes: any;
    name: any;
    description: any;
}