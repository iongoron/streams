export class TransportInstance {
    _id: string;
    provider: string;
    supported_product_id: number[];
    supported_container_id: number[];
    postal_code_start: number;
    postal_code_end: number;
    postal_code_area: string;
    availability: any;
}