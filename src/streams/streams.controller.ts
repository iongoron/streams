import { Controller, Get, Param, Post } from '@nestjs/common';
import { Stream } from 'src/db/entities/stream.entity';
import { StreamsService } from './streams.service';
import { InsertResult } from 'typeorm';

@Controller('streams')
export class StreamsController {
    constructor(private readonly streamsService: StreamsService) { }

    @Get()
    async getAll(): Promise<Stream[]> {
        return this.streamsService.getAll();
    }
    @Get('count')
    async getAllAndCount(): Promise<[Stream[], number]> {
        return this.streamsService.getAllAndCount();
    }

    @Get('get/:areacode')
    getAvailableStreams(@Param('areacode') areacode): Promise<Stream[]> {

        return this.streamsService.getAvailableStreams(areacode);
    }

    @Post('stream')
    async addStream(): Promise<InsertResult> {
        return this.streamsService.addStream();
    }

    @Post('container')
    async addContainer(): Promise<InsertResult> {
        return this.streamsService.addContainer();
    }

    @Post('transport')
    async addTransport(): Promise<InsertResult> {
        return this.streamsService.addTransport();
    }

}
