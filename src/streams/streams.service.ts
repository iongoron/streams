import { Injectable, Response } from '@nestjs/common';
import { InjectRepository, TypeOrmModule } from '@nestjs/typeorm';
import { StreamInstance } from 'src/db/dto/stream.dto';
import { ContainerInstance } from 'src/db/dto/container.dto';
import { TransportInstance } from 'src/db/dto/transport.dto';
import { InsertResult, LessThanOrEqual, MoreThanOrEqual, Repository } from 'typeorm';
import { Stream } from '../db/entities/stream.entity';
import { Container } from '../db/entities/container.entity';
import { Transport } from '../db/entities/transport.entity';
import Utils from 'src/utils/uils';

@Injectable()
export class StreamsService {
    constructor(
        @InjectRepository(Container)
        private containersRepository: Repository<Container>,
        @InjectRepository(Stream)
        private streamRepository: Repository<Stream>,
        @InjectRepository(Transport)
        private transportRepository: Repository<Transport>,
    ) { }

    getAll(): Promise<Stream[]> {
        return this.streamRepository.find();
    }

    getAllAndCount(): Promise<[Stream[], number]> {
        return this.streamRepository.findAndCount();
    }

    async getAvailableStreams(userAreaCode: number): Promise<Stream[]> {
        let streams = await this.findStreamsByArea(userAreaCode),
            transport = await this.findTransportByArea(userAreaCode),
            containers = await this.getAllContainers();

        streams.forEach((stream) => {
            transport.forEach((carrier) => {
                if (carrier.supported_product_id.includes(stream.stream_product_id)) {
                    let availableContainers = containers.filter(container => carrier.supported_container_id.includes(container.container_product_id));

                    stream.sizes.push(containers[0])
                    stream.sizes = Utils.arrayUniqueByParam('container_product_id', stream.sizes.concat(availableContainers));
                }
            })
            if (stream.sizes.length === 0) {
                stream.sizes.push('No transport available');
            }
        });
        return streams;
    }

    async findStreamsByArea(userAreaCode: number): Promise<Stream[]> {
        return this.streamRepository.find(
            {
                where: { areacode: userAreaCode }
            }
        );
    }

    async getAllContainers(): Promise<Container[]> {
        return await this.containersRepository.find();
    }

    async findTransportByArea(userAreaCode: number): Promise<Transport[]> {
        return this.transportRepository.find(
            {
                where: {
                    postal_code_start: LessThanOrEqual(userAreaCode),
                    postal_code_end: MoreThanOrEqual(userAreaCode)
                }
            }
        );
    }

    async addStream(): Promise<InsertResult> {
        let el: StreamInstance = new StreamInstance(),
            hourIncrement = Math.floor(Math.random() * 9) + 1,
            startInterval = new Date(),
            endInterval,
            typeOptions: string[] = ['paper', 'glass', 'PMD', 'fruit', 'Plastic', 'metal', 'drink cartons', 'oil', 'polystyrene', 'PLA'];

        startInterval.setHours(startInterval.getHours() + hourIncrement);
        endInterval = startInterval;
        endInterval.setHours(endInterval.getHours() + 2);

        el.unit_weight = Math.floor(Math.random() * 9) + 1;
        el.type = typeOptions[Math.floor(Math.random() * typeOptions.length)]
        el.stream_product_id = typeOptions.indexOf(el.type);
        el.areacode = 1000 + (Math.floor(Math.random() * 900) + 100);
        el.postal_code_area = 'AP';
        el.sizes = [];
        el.availability = {
            6: [{
                start: startInterval,
                end: endInterval
            }],
            1: [{
                start: startInterval,
                end: endInterval
            }],
            5: [{
                start: startInterval,
                end: endInterval
            }],
            3: [{
                start: startInterval,
                end: endInterval
            }],
        }
        el.description = {
            "en-gb": `${el.type} waste`,
            "nl-nl": `${el.type} waste`,
        };
        el.name = {
            "en-gb": el.type.charAt(0).toUpperCase() + el.type.slice(1),
            "nl-nl": el.type.charAt(0).toUpperCase() + el.type.slice(1),
        };
        let res = await this.streamRepository.insert(el);
        return res;

    }

    async addContainer(): Promise<InsertResult> {
        let el: ContainerInstance = new ContainerInstance(),
            typeOptions: string[] = ['rollcontainer', 'Emmer', 'bag', 'container'],
            sizeArray = [60, 200, 240, 1100, 1700, 12000],
            imageArray: string[] = [
                "https://d39t4x71zbx2q8.cloudfront.net/containers/Seenons-PapierKarton-240.png",
                "https://d39t4x71zbx2q8.cloudfront.net/containers/Seenons-PapierKarton-Bundle.png",
                "https://d39t4x71zbx2q8.cloudfront.net/containers/Seenons-Presscontainer.png",
                "https://d39t4x71zbx2q8.cloudfront.net/containers/Seenons-Presscontainer.png",
                "https://d39t4x71zbx2q8.cloudfront.net/containers/Seenons-LargeContainer.png",
                "https://d39t4x71zbx2q8.cloudfront.net/containers/Seenons-Restafval-1100.png"
            ];

        el.type = typeOptions[Math.floor(Math.random() * typeOptions.length)]
        el.size = sizeArray[Math.floor(Math.random() * sizeArray.length)]
        el.size_display = `${el.size.toString()}l`;
        el.container_product_id = Math.floor(Math.random() * 7) + 1;
        el.image = imageArray[Math.floor(Math.random() * imageArray.length)]
        el.images = [];
        el.description = {
            "en-gb": `${el.size_display} ${el.type}`,
            "nl-nl": `${el.size_display} ${el.type}`,
        };
        el.name = {
            "en-gb": el.type.charAt(0).toUpperCase() + el.type.slice(1),
            "nl-nl": el.type.charAt(0).toUpperCase() + el.type.slice(1),
        };
        let res = await this.containersRepository.insert(el);

        return res;
    }

    async addTransport(): Promise<InsertResult> {
        let el: TransportInstance = new TransportInstance(),
            hourIncrement = Math.floor(Math.random() * 9) + 1,
            startInterval = new Date(),
            endInterval;

        startInterval.setHours(startInterval.getHours() + hourIncrement);
        endInterval = startInterval;
        endInterval.setHours(endInterval.getHours() + 2);

        el.provider = 'GreenCollect';
        el.supported_product_id = [6, 7, 9, 10];
        el.supported_container_id = [1, 2, 3];
        el.postal_code_start = 1000;
        el.postal_code_end = 1099;
        el.postal_code_area = 'AP';
        el.availability = {
            0: [{
                start: startInterval,
                end: endInterval,
                obs: null
            }],
            2: [{
                start: startInterval,
                end: endInterval,
                obs: null
            }],
            4: [{
                start: startInterval,
                end: endInterval,
                obs: 'Every second Friday'
            }],
        }
        let res = await this.transportRepository.insert(el);
        return res;
    }

}
