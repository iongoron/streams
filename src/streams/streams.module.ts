import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Stream } from 'src/db/entities/stream.entity';
import { Container } from 'src/db/entities/container.entity';
import { Transport } from 'src/db/entities/transport.entity';
import { StreamsController } from './streams.controller';
import { StreamsService } from './streams.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([Stream, Container, Transport])
    ],
    exports: [TypeOrmModule],
    controllers: [StreamsController],
    providers: [StreamsService],
})
export class StreamsModule { }
