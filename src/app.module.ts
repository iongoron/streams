import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppService } from './app.service';
import { StreamsController } from './streams/streams.controller';
import { StreamsService } from './streams/streams.service';
import { Stream } from './db/entities/stream.entity';
import { Container } from './db/entities/container.entity';
import { Transport } from './db/entities/transport.entity';
import { StreamsModule } from './streams/streams.module';

@Module({
  imports: [TypeOrmModule.forRoot(
    {
      "type": "mysql",
      "host": "localhost",
      "port": 3306,
      "username": "test",
      "password": "test",
      "database": "wastemanager",
      "entities": [Stream, Container, Transport],
      "synchronize": true
    }
  ), StreamsModule],

  controllers: [AppController, StreamsController],
  providers: [AppService, StreamsService],
})
export class AppModule {
}
